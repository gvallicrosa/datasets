#!/usr/bin/env python

"""
Create a bag from the Marina dataset.

Create a bag with the most standard messages and the transforms required.
"""

from __future__ import print_function

import os
import numpy as np
import rosbag
import rospy

import cola2_lib.NED as NED
import scipy.ndimage

from geometry_msgs.msg import TransformStamped
from tf2_msgs.msg import TFMessage

from cola2_msgs.msg import TeledyneExplorerDvl, ValeportSoundVelocity
from nav_msgs.msg import Odometry
from sensor_msgs.msg import Imu, LaserScan, NavSatFix

from tf.transformations import quaternion_from_euler


def ddmm2deg(string):
    """Convert from DDMM.MMM to radians."""
    left, right = string.split('.')
    deg = float(left[:-2])
    mmm = float("{}.{}".format(left[-2:], right))
    deg += mmm / 60.0
    return deg


def transform(trans, rotat, child_frame_id):
    """Create a TransformStamped message."""
    tfst = TransformStamped()
    tfst.header.frame_id = "ictineu"
    tfst.child_frame_id = child_frame_id
    tfst.transform.translation.x = trans[0]
    tfst.transform.translation.y = trans[1]
    tfst.transform.translation.z = trans[2]
    quat = quaternion_from_euler(np.deg2rad(rotat[0]),
                                 np.deg2rad(rotat[1]),
                                 np.deg2rad(rotat[2]))
    tfst.transform.rotation.x = quat[0]
    tfst.transform.rotation.y = quat[1]
    tfst.transform.rotation.z = quat[2]
    tfst.transform.rotation.w = quat[3]
    return tfst


def create_bag(folder_in, folder_out):
    """Main function to create the bagfile."""
    # Open bag
    bagname = '{:s}/marina.bag'.format(folder_out)
    bag = rosbag.Bag(bagname, "w")
    # Add each sensor
    add_dgps(folder_in, bag, bagname)
    add_dvl(folder_in, bag)
    add_mti(folder_in, bag)
    add_is(folder_in, bag)
    # Close bag
    bag.close()


def add_dgps(folder, bag, bagname):
    """Add info from the *_DGPS.log"""
    # Ground truth output
    gtfh = open("{:s}.gt".format(bagname), "w")
    gpsfh = open("{:s}.gps".format(bagname), "w")
    # DGPS file
    fname = "{:s}/_040825_1735_DGPS.log".format(folder)
    lines = open(fname, "r").readlines()
    trans = [0.0, 0.0, -0.5]
    rotat = [0.0, 0.0, 0.0]  # rpy
    tfst = transform(trans=trans, rotat=rotat, child_frame_id="gps")
    ned = None
    for line in lines:
        # Avoid comments and empty lines
        if line.startswith("%"):
            continue
        if len(line) == 0:
            continue

        # Process data
        data = line.split()
        if not data:
            continue
        tim = rospy.Time(float(data[0]))

        # Transform data
        tfst.header.stamp = tim
        tfmsg = TFMessage()
        tfmsg.transforms.append(tfst)
        bag.write("/tf", tfmsg, tim)

        # GPS data
        msg = NavSatFix()
        msg.header.stamp = tim
        msg.header.frame_id = "gps"
        msg.status.status = msg.status.STATUS_FIX  # check
        msg.status.service = msg.status.SERVICE_GPS  # check
        msg.latitude = ddmm2deg(data[1])
        msg.longitude = ddmm2deg(data[2])
        msg.altitude = float(data[4])
        msg.position_covariance = [0.0] * 9
        msg.position_covariance_type = msg.COVARIANCE_TYPE_UNKNOWN
        bag.write("/cola2_navigation/dgps", msg, tim)

        # GPS csv file
        line = "{:.2f},{:.10f},{:.10f},{:.2f}\n".format(
            tim.to_sec(), msg.latitude, msg.longitude, msg.altitude)
        gpsfh.write(line)

        # Position in ned
        if ned is None:
            ned = NED.NED(msg.latitude, msg.longitude, 0.0)
        xyz = ned.geodetic2ned([msg.latitude, msg.longitude, 0.0])
        yaw_true = np.deg2rad(float(data[6]))
        yaw_mag = np.deg2rad(float(data[7]))
        msg = Odometry()
        msg.header.stamp = tim
        msg.header.frame_id = "world"
        msg.child_frame_id = "dgps"
        msg.pose.pose.position.x = xyz[0]
        msg.pose.pose.position.y = xyz[1]
        msg.pose.pose.position.z = 0.0
        quat = quaternion_from_euler(0.0, 0.0, yaw_mag)  # magnetic
        msg.pose.pose.orientation.x = quat[0]
        msg.pose.pose.orientation.y = quat[1]
        msg.pose.pose.orientation.z = quat[2]
        msg.pose.pose.orientation.w = quat[3]
        bag.write("/cola2_navigation/gps_pose", msg, tim)

        # GT file [time, x, y, z, true course, magnetic course]
        line = "{:.2f},{:.2f},{:.2f},0.0,{:.3f},{:.3f}\n".format(
            tim.to_sec(), xyz[0], xyz[1], yaw_true, yaw_mag)
        gtfh.write(line)

        # Also orientation from DGPS
        if int(data[3]) == 2:
            pass
    gtfh.close()
    gpsfh.close()


def add_dvl(folder, bag):
    """Add info from the *_DVL.log"""
    # Sontek Argonaut DVL file
    fname = "{:s}/_040825_1735_DVL.log".format(folder)
    lines = open(fname, "r").readlines()
    trans = [0.0, 0.0, 0.0]
    rotat = [180.0, 0.0, 60.0]
    tfst = transform(trans=trans, rotat=rotat, child_frame_id="dvl")
    tfvl = transform(trans=trans, rotat=[0, 0, 0], child_frame_id="valeport")
    for line in lines:
        # Avoid comments and empty lines
        if line.startswith("%"):
            continue
        if len(line) == 0:
            continue

        # Process data
        data = line.split()
        if not data:
            continue
        tim = rospy.Time(float(data[0]))

        # Transform data
        tfst.header.stamp = tim
        tfvl.header.stamp = tim
        tfmsg = TFMessage()
        tfmsg.transforms.append(tfst)
        tfmsg.transforms.append(tfvl)
        bag.write("/tf", tfmsg, tim)

        # DVL data
        status = ["B", "A"]
        msg = TeledyneExplorerDvl()
        msg.header.stamp = tim
        msg.header.frame_id = "dvl"
        msg.temperature = float(data[25])
        dbar = 0.00377225 * (float(data[26]) - 1440.0)
        msg.depth = dbar  # 1 dbar = 1 m depth
        msg.wi_x_axis = float(data[7]) * 0.01
        msg.wi_y_axis = float(data[8]) * 0.01
        msg.wi_z_axis = float(data[9]) * 0.01
        msg.wi_status = status[int(data[10])]  # 0: no, 1: ok
        msg.wi_error = -30
        msg.bi_x_axis = float(data[11]) * 0.01
        msg.bi_y_axis = float(data[12]) * 0.01
        msg.bi_z_axis = float(data[13]) * 0.01
        msg.bi_status = status[int(data[14])]  # 0: no, 1: ok
        msg.bi_error = -30
        msg.bd_range = np.mean(
            [float(data[15]), float(data[16]), float(data[17])])
        bag.write("/cola2_navigation/teledyne_explorer_dvl", msg, tim)

        # Pressure data
        msg = ValeportSoundVelocity()
        msg.header.stamp = tim
        msg.header.frame_id = "valeport"
        msg.temperature = float(data[25])
        msg.pressure = dbar
        msg.sound_velocity = 1500.0
        bag.write("/cola2_navigation/valeport_sound_velocity", msg, tim)

        # Imu data
        msg = Imu()
        msg.header.stamp = tim
        msg.header.frame_id = "dvl"
        quat = quaternion_from_euler(np.deg2rad(float(data[24])),
                                     np.deg2rad(float(data[23])),
                                     np.deg2rad(float(data[22])))
        msg.orientation.x = quat[0]
        msg.orientation.y = quat[1]
        msg.orientation.z = quat[2]
        msg.orientation.w = quat[3]
        bag.write("/cola2_navigation/imu_dvl", msg, tim)


def add_mti(folder, bag):
    """Add info from the *_MTi.log"""
    # Xsens MTi file
    fname = "{:s}/_040825_1735_MTi.log".format(folder)
    lines = open(fname, "r").readlines()
    trans = [0.0, 0.0, -0.04]
    rotat = [0.0, 0.0, 90.0]  # rpy
    cov_ang = 0.2 ** 2
    cov_vang = 0.2 ** 2
    tfst = transform(trans=trans, rotat=rotat, child_frame_id="imu")
    for line in lines:
        # Avoid comments and empty lines
        if line.startswith("%"):
            continue
        if len(line) == 0:
            continue

        # Process data
        data = line.split()
        if not data:
            continue
        tim = rospy.Time(float(data[0]))

        # Transform data
        tfst.header.stamp = tim
        tfmsg = TFMessage()
        tfmsg.transforms.append(tfst)
        bag.write("/tf", tfmsg, tim)

        # Imu data
        msg = Imu()
        msg.header.stamp = tim
        msg.header.frame_id = "imu"
        quat = quaternion_from_euler(np.deg2rad(float(data[1])),
                                     np.deg2rad(float(data[2])),
                                     np.deg2rad(float(data[3])))
        msg.orientation.x = quat[0]
        msg.orientation.y = quat[1]
        msg.orientation.z = quat[2]
        msg.orientation.w = quat[3]
        msg.orientation_covariance[0] = cov_ang
        msg.orientation_covariance[4] = cov_ang
        msg.orientation_covariance[8] = cov_ang
        msg.angular_velocity.x = float(data[4]) * 0.1
        msg.angular_velocity.y = float(data[5]) * 0.1
        msg.angular_velocity.z = float(data[6]) * 0.1
        msg.angular_velocity_covariance[0] = cov_vang
        msg.angular_velocity_covariance[4] = cov_vang
        msg.angular_velocity_covariance[8] = cov_vang
        msg.linear_acceleration.x = float(data[7])
        msg.linear_acceleration.y = float(data[8])
        msg.linear_acceleration.z = float(data[9])
        bag.write("/cola2_navigation/imu_mti", msg, tim)


def add_is(folder, bag):
    """Add info from the *_IS.log"""
    # Tritech Miniking Imaging Sonar
    fname = "{:s}/_040825_1735_IS.log".format(folder)
    lines = open(fname, "r").readlines()
    # Transform
    trans = [0.33, 0.0, -0.26]
    rotat = [0.0, 0.0, 180.0]  # rpy
    tfst = transform(trans=trans, rotat=rotat, child_frame_id="micron")
    for line in lines:
        # Avoid comments and empty lines
        if line.startswith("%"):
            continue
        if len(line) == 0:
            continue

        # Process data
        data = line.split()
        if not data:
            continue
        tim = rospy.Time(float(data[0]))

        # Transform data
        tfst.header.stamp = tim
        tfmsg = TFMessage()
        tfmsg.transforms.append(tfst)
        bag.write("/tf", tfmsg, tim)

        # Micron data
        msg = LaserScan()
        msg.header.stamp = tim
        msg.header.frame_id = "micron"
        msg.angle_min = float(data[2])  # * math.pi / 180.0 bad documented
        msg.angle_max = msg.angle_min
        msg.angle_increment = 0.0
        msg.time_increment = 0.0
        msg.scan_time = 0.0
        msg.range_min = 0.0
        msg.range_max = 50.0
        msg.ranges = [0.1 * (i + 1) for i in range(500)]
        msg.intensities = [int(v) for v in data[3:]]
        bag.write("/micron", msg, tim)
        num = 40
        intens = scipy.ndimage.filters.convolve(
            msg.intensities,
            np.full((num,), 1.0 / num), mode='constant')
        msg.intensities = intens
        bag.write("/micron_filtered", msg, tim)


if __name__ == "__main__":
    # Gather input and output folders
    dirin = os.path.dirname(os.path.realpath(__file__))
    dirout = os.getcwd()
    dirout = '.' if dirout == '' else dirout
    print("Input dir:  {:s}".format(dirin))
    print("Output dir: {:s}".format(dirout))
    # Create bag
    create_bag(dirin, dirout)

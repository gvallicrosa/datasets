#!/usr/bin/env python

"""
Find ground truth positions.

Use certain images with available calibration.
"""

from __future__ import print_function
import os
import sys
import cv2
import rosbag
import numpy as np
from cv_bridge import CvBridge

# exact times when each cone is better seen
times = [
    1372687223.0207,
    1372687283.2187,
    1372687384.8176,

    1372687429.8187,
    1372687570.4181,
    1372687895.8753,

    1372688414.0869,
    1372688477.8873,
    1372688594.4861,

    1372688650.0856,
    1372689028.0862,
    1372689151.2855
]

# calibration taken from aggelos code
# intrinsics
mtx = np.array([
    [405.63847389, 0.0, 189.90543179],
    [0.0, 405.58833538, 139.91495783],
    [0.0, 0.0, 1.]
])
# distortion
dist = np.array([-0.367065623341692, 0.203001968694465,
                 0.003336917744124, -0.000487426354680, 0.0])


class Finder(object):
    """Find distance to center of cones."""

    def __init__(self):
        """Constructor."""
        self.points = list()
        self.solution = None

    def find_position(self, img, mtx):
        """Find cone position in camera frame."""
        self.img = img.copy()
        self.mtx = mtx
        self.imtx = np.linalg.inv(mtx)
        cv2.namedWindow("image")
        cv2.setMouseCallback("image", self.callback)
        self.done = False
        print("click on points and right click to finish")
        while not self.done:
            cv2.imshow('image', self.img)
            cv2.waitKey(1)
        return self.solution

    def callback(self, event, x, y, flags, param):
        """Mouse callback."""
        # gather points
        if event == cv2.EVENT_LBUTTONDOWN:
            self.points.append(np.array([x, y, 1.0]))
            cv2.circle(self.img, (x, y), 1, (0, 255, 0), -1)
            cv2.imshow('image', self.img)
            # enough points
            if len(self.points) == 4:
                self.compute_position()
                self.done = True
        # exit
        # elif event == cv2.EVENT_MBUTTONDOWN:
            # self.done = True

    def compute_position(self):
        """Compute center position of the cone."""
        rays = [self.imtx.dot(pt) for pt in self.points]
        i = 0
        j = 1
        ds = list()
        for _ in range(4):
            # check distance between rays
            r1 = rays[i]
            r2 = rays[j]
            ca = r1.dot(r2) / np.linalg.norm(r1) / np.linalg.norm(r2)
            sa = np.sin(np.arccos(ca))
            ds.append(0.2 / sa)  # proportional to real distance 20x20cm cones
            # next
            i = (i + 1) % 4
            j = (j + 1) % 4
        # check
        z = np.mean(ds)
        s = np.std(ds)
        # print("dists:", ds)
        print("z: {:.3f} + {:.3f} sigma".format(z, s))
        i = 0
        j = 1
        ds = list()
        self.solution = np.zeros(3)
        for _ in range(4):
            # check distance between rays
            r1 = rays[i] * z
            r2 = rays[j] * z
            uv1 = self.mtx.dot(r1)
            uv1 /= uv1[2]
            uv2 = self.mtx.dot(r2)
            uv2 /= uv2[2]
            cv2.line(self.img, (int(uv1[0]), int(uv1[1])), (int(
                uv2[0]), int(uv2[1])), (255, 0, 0), 1)
            # center
            self.solution += r1
            # next
            i = (i + 1) % 4
            j = (j + 1) % 4
        self.solution /= 4.0
        uv1 = self.mtx.dot(self.solution)
        uv1 /= uv1[2]
        cv2.circle(self.img, (int(uv1[0]), int(uv1[1])), 1, (0, 0, 255), -1)
        cv2.imshow("image", self.img)


def main():
    """Extract a folder of images from a rosbag."""
    # Input
    if len(sys.argv) < 2:
        print("Usage:\n  {:s} rosbag".format(sys.argv[0]))
        return
    # Params
    topic = '/camera/image_raw'
    bagfile = sys.argv[1]
    folder = os.path.dirname(bagfile)
    fname = os.path.basename(bagfile)
    print("folder:", folder)
    print("file:", fname)
    # Output file
    fh = open(fname + ".txt", "w")
    # Go through the bag
    bag = rosbag.Bag(bagfile, "r")
    bridge = CvBridge()
    for topic, msg, t in bag.read_messages(topics=[topic]):
        for ct in times:
            if abs(t.to_sec() - ct) < 1e-2:
                # image in opencv
                img = bridge.imgmsg_to_cv2(msg, desired_encoding="passthrough")
                # save to disk
                fname = "cone{:.4f}.png".format(t.to_sec())
                cv2.imwrite(os.path.join(folder, fname), img)
                # undistort
                # http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html
                h, w = img.shape[:2]
                newmtx, roi = cv2.getOptimalNewCameraMatrix(
                    mtx, dist, (w, h), 1, (w, h))
                dst = cv2.undistort(img, mtx, dist, None, newmtx)
                x, y, w, h = roi  # crop the image
                dst = dst[y:y + h, x:x + w]
                fname = "cone{:.4f}undist.png".format(t.to_sec())
                cv2.imwrite(fname, dst)
                # find points
                finder = Finder()
                xyz = finder.find_position(dst, newmtx)
                fname = "cone{:.4f}undist-loc.png".format(t.to_sec())
                cv2.imwrite(fname, finder.img)
                # save to output file [time x y z] from the camera frame
                line = "{:.3f} {:.3f} {:.3f} {:.3f}\n".format(
                    ct, xyz[0], xyz[1], xyz[2])
                fh.write(line)
    # Close
    bag.close()
    fh.close()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()

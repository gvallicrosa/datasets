#!/usr/bin/env python

"""Show profiler data."""

from __future__ import print_function
import json
import sys
import rosbag
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

# TODO: show min intensity

# configuration
min_rng = 0.85
max_rng = 9.0  # 10.0
min_int = 80.0


def main():
    """Main function to call."""
    # init figure
    plt.ion()
    _, [ax1, ax2] = plt.subplots(1, 2, figsize=(14, 7))

    # input
    bagfile = sys.argv[1]
    jdata = json.load(open("{:s}.json".format(bagfile), 'r'))
    time_start = jdata["bag"]["time"]["start"]
    time_stop = jdata["bag"]["time"]["stop"]

    # read bag
    topic = '/profiler/beam'
    endangle = None
    beams = list()
    bag = rosbag.Bag(bagfile, "r")
    for topic, msg, t in bag.read_messages(topics=[topic]):
        # check time
        if t.to_sec() < time_start:
            continue
        elif t.to_sec() > time_stop:
            break

        # save end angle or show
        if endangle is None:
            endangle = msg.angle_min
        elif endangle == msg.angle_min:
            # compute

            # show
            # plt.cla()
            ax1.cla()
            ax2.cla()
            plot_intensities(beams, ax1)
            plot_guides(beams, ax1)

            plot_segmentation(beams, ax2)
            plot_guides(beams, ax2)

            ax1.axis('equal')
            ax2.axis('equal')
            plt.show()
            # wait
            b = raw_input("press any key ['q' to exit]> ")
            if b and b[0] == 'q':
                break
            # clear
            beams = list()

        # just add the msg
        beams.append(msg)


def plot_intensities(msgs, ax):
    """Plot intensities of each full scan with color and size."""
    # intensities
    points = list()
    for msg in msgs:
        for i, r in enumerate(msg.ranges):
            x = r * np.cos(msg.angle_min + np.pi)
            y = r * np.sin(msg.angle_min + np.pi)
            c = msg.intensities[i]
            points.append([x, y, c])
    points = np.array(points)
    view = points[:, 2]
    view[view < min_int] = 0.0
    ax.scatter(x=points[:, 1], y=points[:, 0],
               s=0.2 * points[:, 2], c=points[:, 2], cmap='jet')


def plot_guides(msgs, ax):
    """Plot the range min and max, and the start and end beams."""
    # circle
    a = np.linspace(0.0, 2 * np.pi, 360)
    x = min_rng * np.cos(a)
    y = min_rng * np.sin(a)
    ax.plot(x, y, 'k-')
    x = max_rng * np.cos(a)
    y = max_rng * np.sin(a)
    ax.plot(x, y, 'k-')
    # zero line
    a = msgs[0].angle_min + np.pi
    x = max_rng * np.cos(a)
    y = max_rng * np.sin(a)
    ax.plot([0.0, x], [0.0, y], 'k-', lw=2)
    # some line
    a = msgs[5].angle_min + np.pi
    x = max_rng * np.cos(a)
    y = max_rng * np.sin(a)
    ax.plot([0.0, x], [0.0, y], 'r-', lw=2)


def threshold(msg):
    """Segmentate a beam."""
    # Search for maximum intensity in between the range limits
    index = 0
    maxint = -1
    for i in range(len(msg.ranges)):
        # in the range limits
        if min_rng < msg.ranges[i] < max_rng:
            # more than min intensity
            if min_int <= msg.intensities[i] and msg.intensities[i] > maxint:
                # new max
                maxint = msg.intensities[i]
                index = i
    # result free or occupied
    if index > 0:
        return msg.ranges[index], msg.intensities[index]
    # no range
    return 0.0, 0.0


def plot_segmentation(msgs, ax):
    """Plot only the final range of every beam."""
    points = list()
    for msg in msgs:
        r, i = threshold(msg)
        x = r * np.cos(msg.angle_min + np.pi)
        y = r * np.sin(msg.angle_min + np.pi)
        c = i
        points.append([x, y, c])
    # show
    points = np.array(points)
    ax.scatter(x=points[:, 1], y=points[:, 0],
               s=0.4 * points[:, 2], c='r', cmap='Purples')
    # rays
    for point in points:
        ax.plot([0.0, point[1]], [0.0, point[0]], 'k-')

if __name__ == '__main__':
    main()

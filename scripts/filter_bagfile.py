#!/usr/bin/env python

"""Utility to filter bags to have only the essential."""

from __future__ import print_function
import sys
import rosbag


def main():
    """Main function."""
    # check inputs
    if len(sys.argv) < 3:
        print("usage:\n  {:s} inbag [inbag2 ...] outbag".format(sys.argv[0]))
        exit(0)

    # process
    outbag = rosbag.Bag(sys.argv[-1], 'w')
    for name in sys.argv[1:-1]:
        print("process {:s}...".format(name))
        save_filtered(name, outbag)
    print("saved to {:s}".format(sys.argv[-1]))
    outbag.close()


def save_filtered(inbagname, outbag):
    """Save certain topics from a bag to the output bag."""
    topics = (
        "/cola2_navigation/fastrax_it_500_gps",
        "/cola2_navigation/gps_pose",
        "/cola2_navigation/imu",
        "/cola2_navigation/nav_sts",
        "/cola2_navigation/pressure_sensor",
        "/cola2_navigation/teledyne_explorer_dvl",
        "/cola2_perception/altitude",
        "/params_string",
        "/pose_ekf_slam/odometry",
        "/tf",
        "/tritech_seaking_micron_beam",
        "/profiler/parameter_updates",
        "/tritech_seaking_profiler_beam",
        "/micron/parameter_updates"
    )
    bag = rosbag.Bag(inbagname, 'r')
    for topic, msg, t in bag.read_messages(topics=topics):
        # Write to output
        outbag.write(topic, msg, t)
    bag.close()


if __name__ == '__main__':
    main()
